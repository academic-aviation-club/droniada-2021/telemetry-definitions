DEFINITIONS_FILE=definitions.proto
PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"
OUT_DIR="./generated"

all: pre_build python typescript

typescript: generated/definitions_pb.js 
python: generated/definitions_pb2.py

node_modules: 
	npm install ts-protoc-gen typescript

venv:
	virtualenv venv
	. venv/bin/activate; pip install protobuf

pre_build:
	mkdir -p $(OUT_DIR)

generated/definitions_pb2.py: venv $(DEFINITIONS_FILE)
	protoc -I . $(DEFINITIONS_FILE) --python_out=$(OUT_DIR)

generated/definitions_pb.js: node_modules $(DEFINITIONS_FILE)
	protoc \
    --plugin="protoc-gen-ts=${PROTOC_GEN_TS_PATH}" \
    --js_out="import_style=commonjs,binary:${OUT_DIR}" \
    --ts_out="${OUT_DIR}" \
    $(DEFINITIONS_FILE)

test: all
	./venv/bin/python test.py;
	rm -rf test.js
	tsc test.ts;
	bash test.sh
	@echo
	@echo All good
	
clear:
	rm -rf $(OUT_DIR) venv node_modules package-lock.json test.js msg.bin