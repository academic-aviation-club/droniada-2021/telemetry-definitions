// package: 
// file: definitions.proto

import * as jspb from "google-protobuf";

export class GpsUpdate extends jspb.Message {
  getNorth(): number;
  setNorth(value: number): void;

  getEast(): number;
  setEast(value: number): void;

  getDown(): number;
  setDown(value: number): void;

  getHeading(): number;
  setHeading(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GpsUpdate.AsObject;
  static toObject(includeInstance: boolean, msg: GpsUpdate): GpsUpdate.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GpsUpdate, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GpsUpdate;
  static deserializeBinaryFromReader(message: GpsUpdate, reader: jspb.BinaryReader): GpsUpdate;
}

export namespace GpsUpdate {
  export type AsObject = {
    north: number,
    east: number,
    down: number,
    heading: number,
  }
}

export class ObjectDetection extends jspb.Message {
  getObjectId(): number;
  setObjectId(value: number): void;

  getDetectedObject(): DetectedObjectMap[keyof DetectedObjectMap];
  setDetectedObject(value: DetectedObjectMap[keyof DetectedObjectMap]): void;

  getNorth(): number;
  setNorth(value: number): void;

  getEast(): number;
  setEast(value: number): void;

  getDown(): number;
  setDown(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ObjectDetection.AsObject;
  static toObject(includeInstance: boolean, msg: ObjectDetection): ObjectDetection.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ObjectDetection, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ObjectDetection;
  static deserializeBinaryFromReader(message: ObjectDetection, reader: jspb.BinaryReader): ObjectDetection;
}

export namespace ObjectDetection {
  export type AsObject = {
    objectId: number,
    detectedObject: DetectedObjectMap[keyof DetectedObjectMap],
    north: number,
    east: number,
    down: number,
  }
}

export class DroniadaMessage extends jspb.Message {
  getFlightId(): number;
  setFlightId(value: number): void;

  hasGpsUpdate(): boolean;
  clearGpsUpdate(): void;
  getGpsUpdate(): GpsUpdate | undefined;
  setGpsUpdate(value?: GpsUpdate): void;

  hasObjectDetection(): boolean;
  clearObjectDetection(): void;
  getObjectDetection(): ObjectDetection | undefined;
  setObjectDetection(value?: ObjectDetection): void;

  getMessagesCase(): DroniadaMessage.MessagesCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DroniadaMessage.AsObject;
  static toObject(includeInstance: boolean, msg: DroniadaMessage): DroniadaMessage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DroniadaMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DroniadaMessage;
  static deserializeBinaryFromReader(message: DroniadaMessage, reader: jspb.BinaryReader): DroniadaMessage;
}

export namespace DroniadaMessage {
  export type AsObject = {
    flightId: number,
    gpsUpdate?: GpsUpdate.AsObject,
    objectDetection?: ObjectDetection.AsObject,
  }

  export enum MessagesCase {
    MESSAGES_NOT_SET = 0,
    GPS_UPDATE = 2,
    OBJECT_DETECTION = 3,
  }
}

export interface DetectedObjectMap {
  TRIANGLE: 0;
  CIRCLE: 1;
  SQUARE: 2;
}

export const DetectedObject: DetectedObjectMap;

